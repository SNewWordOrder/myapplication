package com.example.student.myapplication;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;


/**
 * A simple {@link Fragment} subclass.
 */
public class RegFrag extends Fragment {
private EditText usernamefield;
    private EditText passwordfield;

    public RegFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.activity_2, container, false);
        Button button = (Button) view.findViewById(R.id.RegisterButton);
        usernamefield = (EditText) view.findViewById(R.id.usernamefield);
        passwordfield = (EditText) view.findViewById(R.id.passwordfield);
        button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
String username = usernamefield.getText().toString();
                String password = passwordfield.getText().toString();
                BackendlessUser backendlessUser = new BackendlessUser();
                backendlessUser.setPassword(password);
                backendlessUser.setProperty("name", username);
                Backendless.UserService.register(backendlessUser, new AsyncCallback<BackendlessUser>() {
                    @Override
                    public void handleResponse(BackendlessUser response) {
                        Toast.makeText(getActivity(), "Yes", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {
                        Toast.makeText(getActivity(), "No", Toast.LENGTH_SHORT).show();
                    }
                }

                }

                }




        ;return view;
    }



